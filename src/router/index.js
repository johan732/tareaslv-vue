import { createRouter, createWebHistory } from "vue-router";

import Login from "@/components/Login";
import Tarea from "@/components/Tarea";

const routes = [
  { path: "/", component: Login },
  { path: "/tareas", component: Tarea },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routes,
});

export default router;
